
ENT.PrintName		= "FRP NPC Shop Base"
ENT.Author			= "XzaR"
ENT.Contact			= "N/A" 
ENT.Purpose			= "N/A"
ENT.Instructions	= "N/A"

ENT.Base = "base_ai"
ENT.Type = "ai"

ENT.AutomaticFrameAdvance = true

function ENT:CustomInit()

	self:SetMoveType(MOVETYPE_STEP)
	local sequence = self:LookupSequence("idle");
	self:ResetSequence(sequence);
	
	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal( )
	self:SetNPCState( NPC_STATE_IDLE )
	self:SetSolid(  SOLID_BBOX )
	self:SetCollisionGroup(COLLISION_GROUP_NPC)
	self:CapabilitiesAdd( bit.bor( CAP_ANIMATEDFACE, CAP_TURN_HEAD,CAP_INNATE_MELEE_ATTACK1, CAP_INNATE_MELEE_ATTACK2,CAP_DUCK ) )
	self:SetUseType( SIMPLE_USE )
	self:DropToFloor()
	self:SetHealth(50)
	
end

function ENT:Initialize( )
	
	self:SetModel( "models/player/Group01/Female_01.mdl" )
	self:CustomInit();
	
end