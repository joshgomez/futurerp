AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')

ENT.died = false

function ENT:Think()
        self:SetSequence(self:LookupSequence("idle_all_01"))
end

 function ENT:OnTakeDamage(dmg)
 
	self:SetHealth(self:Health() - dmg:GetDamage());
	if(!self.died and self:Health() <= 0) then
	
		self.died = true;
		
		local inflictor = dmg:GetInflictor()
		local attacker = dmg:GetAttacker()
		gamemode.Call("OnNPCKilled",self,attacker,inflictor,dmg)
		if attacker:IsPlayer() then attacker:AddFrags(1) end
		
		local ragdoll = ents.Create("prop_ragdoll")
		ragdoll:SetPos(self:GetPos())
		ragdoll:SetAngles(Angle(0,self:GetAngles().Yaw,0))
		ragdoll:SetCollisionGroup(1)
		ragdoll:SetModel(self:GetModel())
		ragdoll:Spawn()
		ragdoll:Activate()
		ragdoll:SetVelocity(self:GetVelocity())
		ragdoll.PhysgunPickup = false
		ragdoll.CanTool = false	
		ragdoll.SetOwner(attacker);
		ragdoll:SetUseType(SIMPLE_USE)
		cleanup.ReplaceEntity(ragdoll)
		
		timer.Simple( 30 , function() 
		
			if(ragdoll:IsValid()) then
				ragdoll:Remove() 
			end
		
		end) 
		
		self:Remove();
		
	end
 end 

function ENT:AcceptInput( name, activator, caller )	

end
