include('shared.lua') -- At this point the contents of shared.lua are ran on the client only.

ENT.RenderGroup = RENDERGROUP_BOTH

local class = "npc_frp_shop_ammo"
local name = FRP.Lists.names[class]

language.Add(class, name)
killicon.Add(class,"HUD/killicons/default",Color ( 255, 80, 0, 255 ) )
language.Add("#"..class, name)
killicon.Add("#"..class,"HUD/killicons/default",Color ( 255, 80, 0, 255 ) )