include('shared.lua')

ENT.RenderGroup = RENDERGROUP_BOTH

local class = "npc_frp_shop_armor"
local name = FRP.Lists.names[class]

language.Add(class, name)
killicon.Add(class,"HUD/killicons/default",Color ( 255, 80, 0, 255 ) )
language.Add("#"..class, name)
killicon.Add("#"..class,"HUD/killicons/default",Color ( 255, 80, 0, 255 ) )
