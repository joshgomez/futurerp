
AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {} 
PLAYER.WalkSpeed 				= 200
PLAYER.RunSpeed				= 400

function PLAYER:Loadout()

	self.Player:Give( "weapon_crowbar" );
	self.Player:Give( "gmod_tool" )
	self.Player:Give( "gmod_camera" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "weapon_physcannon" )
	
end

player_manager.RegisterClass( "player_frp_default", PLAYER, "player_default" )