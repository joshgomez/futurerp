
include( "shared.lua" )

include( "client/fonts.lua" )

include( "client/hooks/HUDPaint.lua" )
include( "client/hooks/HUDShouldDraw.lua" )
include( "client/hooks/OnEntityCreated.lua" )

include( "client/net/ChatCode.lua" )

include( "client/umhooks/npc/shopmenus/weapon.lua" )
include( "client/umhooks/npc/shopmenus/ammo.lua" )
include( "client/umhooks/npc/shopmenus/health.lua" )
include( "client/umhooks/npc/shopmenus/armor.lua" )
include( "client/umhooks/npc/shopmenus/food.lua" )
include( "client/umhooks/BuyStatus.lua" )
include( "client/umhooks/ShowMenu.lua" )

hook.Add( "HUDPaint"							,"frp_HUDPaint"						,function() return FRP:HUDPaint() end)
hook.Add( "HUDShouldDraw"					,"frp_HUDShouldDraw"				,function(name) return FRP:HUDShouldDraw(name) end)
hook.Add( "OnEntityCreated"				,"frp_OnEntityCreated"				,function(ent ) return FRP:OnEntityCreated(ent ) end)

usermessage.Hook("FRP_NPCWeaponShop"			,function(data) return FRP.NPC.ShopMenu:Weapon(data) end)
usermessage.Hook("FRP_NPCAmmoShop"			,function(data) return FRP.NPC.ShopMenu:Ammo(data) end)
usermessage.Hook("FRP_NPCHealthShop"			,function(data) return FRP.NPC.ShopMenu:Health(data) end)
usermessage.Hook("FRP_NPCArmorShop"			,function(data) return FRP.NPC.ShopMenu:Armor(data) end)
usermessage.Hook("FRP_NPCFoodShop"				,function(data) return FRP.NPC.ShopMenu:Food(data) end)
usermessage.Hook("FRP_BuyStatus"					,function(data) return FRP:BuyStatus(data) end)
usermessage.Hook("FRP_ShowMenu"					,function(data) return FRP:ShowMenu(data) end)