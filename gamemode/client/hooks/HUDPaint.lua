--[[ 

	Dependency:
		cl_init.lua
		shared.lua
		shared/player/level.lua
		shared/player/experience.lua
		shared/player/money.lua
		client/fonts.lua

]]

function FRP:HUDPaint()

	 if(!FRP.running) then return end
	 
	 local client = LocalPlayer();
	 
	 if !(client:Alive()) then return end
	 if !(client:GetActiveWeapon() and IsValid(client:GetActiveWeapon())) then return end

	local target = LocalPlayer():GetEyeTrace().Entity;
	  
	local nick 				= client:Nick()
	local level 				= client:Level()
	local nextLevel 		= client:NextLevel()
	local experience 		= client:Experience()
	local fame 				= client:Fame()
	local energy 			= math.Round(client:Energy())
	local hunger 			= math.Round(client:Hunger())
	
	local xp = experience .. "/" .. nextLevel;
	if(target:IsPlayer()) then
		nick 						= target:Nick()
		level 					= target:Level()
		fame 					= target:Fame()
		xp 						= "???";
	end
	
    local health  	= client:Health()
    local armor 		= client:Armor()
	local money 	= client:Money()
	
	local weapon = client:GetActiveWeapon();
	
	local ammoClip = weapon:Clip1();
	local ammoCount = client:GetAmmoCount(weapon:GetPrimaryAmmoType());
	local ammoCount2 = client:GetAmmoCount(weapon:GetSecondaryAmmoType());

	local heightFont = draw.GetFontHeight( "FRP_HUD" )
	
	local sizeW = 116+10;
	local posX = ScrW() - (sizeW+25);
	local posY = ScrH() - (heightFont*3);
	

	local widthWordBox = 0
	widthWordBox = draw.WordBox( 8, 25, heightFont, "Name: ".. nick, "FRP_HUD", Color(100,200,100,230), Color(255,255,255,255))
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8, heightFont, "Level: ".. level, "FRP_HUD", Color(100,200,100,230), Color(255,255,255,255))
	widthWordBox = draw.WordBox( 8, 25+widthWordBox+8*2, heightFont, "Fame: ".. fame, "FRP_HUD", Color(100,200,100,230), Color(255,255,255,255))
	
	widthWordBox = draw.WordBox( 8, 25, heightFont*3+4, "Experience: ".. xp, "FRP_HUD", Color(100,200,100,230), Color(255,255,255,255))
	
	widthWordBox = draw.WordBox( 8, 25, ScrH()-heightFont*3, "Health: ".. health, "FRP_HUD", Color(255,100,100,230), Color(255,255,255,255))
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8, ScrH()-heightFont*3, "Armor: ".. armor, "FRP_HUD", Color(100,100,255,230), Color(255,255,255,255))
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8*2, ScrH()-heightFont*3, "Money: ".. money .. FRP.currency, "FRP_HUD", Color(200,200,100,230), Color(255,255,255,255))
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8*3, ScrH()-heightFont*3, "Energy: ".. energy .. "%", "FRP_HUD", Color(255,100,255,230), Color(255,255,255,255))
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8*4, ScrH()-heightFont*3, "Hunger: ".. hunger .. "%", "FRP_HUD", Color(255,150,100,230), Color(255,255,255,255))
	
	local class = weapon:GetClass();
	local clipSize = FRP.Temp.clipSize[class];
	
	if (not clipSize or ammoClip > clipSize) then
		FRP.Temp.clipSize[class] = ammoClip;
		clipSize = ammoClip;
	end
	
	local ammo = ammoClip .. "/" .. clipSize .. " | ".. ammoCount;
	if(ammoClip < 0) then
			ammo = ammoCount;
	end
	
	widthWordBox = widthWordBox + draw.WordBox( 8, 25+widthWordBox+8*5, ScrH()-heightFont*3, "Ammo: ".. ammo, "FRP_HUD", Color(100,100,100,230), Color(255,255,255,255))

	if(ammoCount2 > 0) then
		widthWordBox = draw.WordBox( 8, 25+widthWordBox+8*6, ScrH()-heightFont*3,ammoCount2, "FRP_HUD", Color(100,100,100,230), Color(255,255,255,255))
	end
	
	
end