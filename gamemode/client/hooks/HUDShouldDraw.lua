--[[ 

	Dependency:
		shared.lua

]]

function FRP:HUDShouldDraw( name )
	for k, v in pairs({"CHudHealth", "CHudBattery", "CHudAmmo", "CHudSecondaryAmmo"}) do
		if name == v then return false end
	end
end