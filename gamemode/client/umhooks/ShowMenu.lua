--[[ 

	Dependency:
		shared.lua

]]

local function ButtonLayout(frame,button,obj)

	local btn = obj or {};
	btn.pos = btn.pos or {0,0};
	btn.size = btn.size or {100,50};
	btn.text = btn.text or "Undefined";
	

	button:SetParent( frame )
	button:SetText( btn.text )
	button:SetSize( btn.size[1], btn.size[2] )
	button:SetPos( btn.pos[1], btn.pos[2])
	button.Paint = function()
	
		draw.RoundedBox( 8, 0, 0, button:GetWide(), button:GetTall(), Color( 200, 200, 200, 200 ) )
	end

end

local function TextSheet(frame,text)

	frame.Paint = function()
	
		draw.RoundedBox( 8, 0, 0, frame:GetWide(), frame:GetTall(), Color( 200, 200, 200, 200 ) )
		draw.DrawText( text, "DermaDefault", 25, 25, Color( 0, 0, 0, 255 ), TEXT_ALIGN_LEFT  ) 
	end
end

function FRP.ShowMenu(data)

	--local propCost = data:ReadString();

	local fW 	= 700;
	local fH 	= ScrH()*0.5;
	local dFrame = vgui.Create('DFrame')
	
	dFrame:SetSize(fW,  fH)
	dFrame:SetPos(ScrW()*0.5-(fW/2), fH/2)
	dFrame:SetTitle("")
	dFrame:SetSizable(false)
	dFrame:SetDraggable( false )
	dFrame:SetDeleteOnClose(false)
	dFrame:MakePopup()
	
	local heightFont = draw.GetFontHeight( "FRP_MenuTitle" )
	
	dFrame.Paint = function()
	
		draw.RoundedBox( 8, 0, 0, dFrame:GetWide(), dFrame:GetTall(), Color( 0, 0, 0, 200 ) )
		draw.SimpleText(FRP.name, "FRP_MenuTitle", dFrame:GetWide()/2, heightFont, Color(0,148,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
		
	end
	
	local sheetW = dFrame:GetWide()/2;
	local dSheet = vgui.Create("DPropertySheet", dFrame);
	dSheet:SetPos(dFrame:GetWide()-(sheetW+25), 50+heightFont);
	dSheet:SetSize(sheetW, dFrame:GetTall()-(50+heightFont+25));
	
	local homeText = FRP.description;
	
	TextSheet(dSheet,homeText);
	
	local btn = {};
	local dButton = {}
	local dTextNick = nil;
	local dTextMoney = nil
	local dButtonGive = nil
	local buttonClicked = {};
	
	--Button
	dButton[1] = vgui.Create( "DButton" )
	btn.pos ={25, 50+heightFont};
	btn.text = "Home";
	
	ButtonLayout(dFrame,dButton[1],btn)
	dButton[1].DoClick = function ()
		buttonClicked[2] = false;
		TextSheet(dSheet,homeText);
		if(dTextNick) then dTextNick:Remove(); end
		if(dTextMoney) then dTextMoney:Remove(); end
		if(dButtonGive) then dButtonGive:Remove(); end
	end
	
	--Button
	dButton[2] = vgui.Create( "DButton" )
	btn.pos ={25, 50*2+heightFont+20};
	btn.text = "Give Money";
	
	ButtonLayout(dFrame,dButton[2] ,btn)
	dButton[2].DoClick = function ()
		
		if(buttonClicked[2]) then return false end
		buttonClicked[2] = true;

		local heightFont = draw.GetFontHeight( "DermaDefault" )
		
		dSheet.Paint = function()
		
			draw.RoundedBox( 8, 0, 0, dSheet:GetWide(), dSheet:GetTall(), Color( 200, 200, 200, 200 ) )
			draw.DrawText( "Nickname:", "DermaDefault", 25, 25, Color( 0, 0, 0, 255 ), TEXT_ALIGN_LEFT  ) 
			draw.DrawText( "Money:", "DermaDefault", 25, 50+heightFont, Color( 0, 0, 0, 255 ), TEXT_ALIGN_LEFT  ) 
			
			if(LocalPlayer():Level() < 5) then
				draw.DrawText( "You must be level 5.", "DermaDefault", 25, 75+heightFont*3+50, Color( 0, 0, 0, 255 ), TEXT_ALIGN_LEFT  )
			end
		end
		
		dTextNick = vgui.Create( "DTextEntry", dSheet )
		dTextNick:SetPos( 25,25+heightFont )
		dTextNick:SetTall( 20 )
		dTextNick:SetWide( dSheet:GetWide()-50 )
		dTextNick:SetEnterAllowed( true )
		
		dTextMoney = vgui.Create( "DTextEntry", dSheet )
		dTextMoney:SetPos( 25,25*2+heightFont*2 )
		dTextMoney:SetTall( 20 )
		dTextMoney:SetWide( dSheet:GetWide()-50 )
		dTextMoney:SetEnterAllowed( true )
		
		dButtonGive = vgui.Create( "DButton" )
		dButtonGive:SetParent( dSheet )
		dButtonGive:SetText( "Give" )
		dButtonGive:SetSize( 100, 50 )
		dButtonGive:SetPos( 25,75+heightFont*3)
		
		if(LocalPlayer():Level() < 5) then
			dButtonGive:SetDisabled(true)
		end
		
		dButtonGive.DoClick = function ()
			RunConsoleCommand("frp_give_money",dTextNick:GetValue(),dTextMoney:GetValue());
			dFrame:Remove();
		end
	end
	
	
	--Button
	dButton[3] = vgui.Create( "DButton" )
	btn.pos ={25, 50*3+heightFont+20*2};
	btn.text = "Drop Weapon";
	
	ButtonLayout(dFrame,dButton[3],btn)
	dButton[3].DoClick = function ()
		RunConsoleCommand("frp_drop_weapon");
		dFrame:Remove();
	end
	
	--Button
	dButton[4] = vgui.Create( "DButton" )
	btn.pos ={25, 50*4+heightFont+20*3};
	btn.text = "Start Over";
	
	ButtonLayout(dFrame,dButton[4],btn)
	dButton[4].DoClick = function ()
		RunConsoleCommand("frp_player_reset");
		dFrame:Remove();
	end
end
