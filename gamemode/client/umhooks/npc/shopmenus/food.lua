--[[ 

	Dependency:
		shared.lua
		shared/util.lua
		shared/lists/prices.lua
		shared/lists/names.lua
		shared/lists/shops.lua

]]

function FRP.NPC.ShopMenu:Food(data)

	local className = data:ReadString();
	local fW = 600;
	local fH 	= ScrH()*0.5;
	local dFrame = vgui.Create('DFrame')
	
	dFrame:SetSize(fW,  fH)
	dFrame:SetPos((ScrW()-(fW+25)), fH/2)
	dFrame:SetTitle(FRP.Lists.names[className])
	dFrame:SetSizable(false)
	dFrame:SetDraggable( false )
	dFrame:SetDeleteOnClose(false)
	dFrame:MakePopup()
	

	local x = 10;
	local y = 50;
	
	for k, v in pairs( FRP.Lists.Prices.food ) do
	
		local DermaButton = vgui.Create( "DButton" )
			DermaButton:SetParent( dFrame )
			DermaButton:SetText( FRP.Lists.names[k].."\n"..FRP:NumberToMoney(v)..FRP.currency .. " " .. FRP.Lists.Shops.food[k]  .. "%" )
			DermaButton:SetPos( x, y )
			DermaButton:SetSize( 150, 50 )
			DermaButton.DoClick = function ()
			
			net.Start( 'FRP_BuyFood' )
				net.WriteString(className)
				net.WriteString(k)
			net.SendToServer()
			
		end
		
		x = x + 154;
		if(x > fW-(150+4+5)) then
			x = 10;
			y = y + 54;
		end
	end

	chat.AddText(Color(255,255,128), FRP.Lists.names[className]..": ",Color(255,255,255), "Welcome, need any food?" )
	
end