--[[ 

	Dependency:
		shared.lua
		shared/util.lua
		shared/lists/prices.lua
		shared/lists/names.lua

]]

function FRP.NPC.ShopMenu:Weapon(data)

	local className = data:ReadString();
	local fW = 600;
	local fH 	= ScrH()*0.5;
	local dFrame = vgui.Create('DFrame')
	
	dFrame:SetSize(fW,  fH)
	dFrame:SetPos((ScrW()-(fW+25)), fH/2)
	dFrame:SetTitle(FRP.Lists.names[className])
	dFrame:SetSizable(false)
	dFrame:SetDraggable( false )
	dFrame:SetDeleteOnClose(false)
	dFrame:MakePopup()
	

	local x = 10;
	local y = 50;
	
	for k, v in pairs( FRP.Lists.Prices.weapon ) do
	
		local DermaButton = vgui.Create( "DButton" )
			DermaButton:SetParent( dFrame ) -- Set parent to our "DermaPanel"
			DermaButton:SetText( FRP.Lists.names[k].."\n"..FRP:NumberToMoney(v)..FRP.currency )
			DermaButton:SetPos( x, y )
			DermaButton:SetSize( 150, 50 )
			DermaButton.DoClick = function ()
			
			net.Start( 'FRP_BuyWeapon' )
				net.WriteString(className)
				net.WriteString(k)
			net.SendToServer()
			
		end
		
		x = x + 154;
		if(x > fW-(150+4+5)) then
			x = 10;
			y = y + 54;
		end
	end

	chat.AddText(Color(255,255,128), FRP.Lists.names[className]..": ",Color(255,255,255), "Welcome to my shop, how can I help you?" )
	
end