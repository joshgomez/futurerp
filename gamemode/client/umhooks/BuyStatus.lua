--[[ 

	Dependency:
		shared.lua
		shared/util.lua
		shared/lists/prices.lua
		shared/lists/names.lua

]]

function FRP:BuyStatus(data)

	local classShop 		= data:ReadString();
	local item 				= data:ReadString();
	local messageCode 	= data:ReadString();
	
	if(messageCode == "NOT_ENOUGH_MONEY") then
		chat.AddText(Color(255,255,128), FRP.Lists.names[classShop]..": ",
			Color(255,255,255), "Ah, did you not have enough money to buy the ", 
			Color(255,0,0), FRP.Lists.names[item],
			Color(255,255,255), ". :(");
	elseif(messageCode == "BOUGHT_ITEM") then
		chat.AddText(Color(255,255,128), FRP.Lists.names[classShop]..": ",
			Color(255,255,255), "Thank you, here is your ", 
			Color(0,0,255), FRP.Lists.names[item],
			Color(255,255,255), ". :)");
	elseif(messageCode == "ITEM_NOT_FOUND") then
		chat.AddText(Color(255,255,128), FRP.Lists.names[classShop]..": ",Color(255,255,255), "Sorry I don't have the thing you are looking for." )
	elseif(messageCode == "ITEM_REQUIRED" or messageCode == "CAN_NOT_BUY") then
		chat.AddText(Color(255,255,128), FRP.Lists.names[classShop]..": ",Color(255,255,255), "Sorry you can't buy this item yet. :(" )	
	elseif(messageCode == "ITEM_ALREADY_HAVE") then
		chat.AddText(Color(255,255,128), FRP.Lists.names[classShop]..": ",Color(255,255,255), "You already have this item." )		
	end
	
end