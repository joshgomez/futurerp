     
	 
function FRP.Fonts()
	
	 surface.CreateFont( "FRP_MenuTitle",
	 {
					font    = "Atomic Age",
					size    = 45,
					weight  = 400,
					antialias = true,
					shadow = false
	  })
	 
	 surface.CreateFont( "FRP_HUD",
	 {
					font    = "Verdana",
					size    = 18,
					weight  = 200,
					antialias = true,
					shadow = false
	  })
end