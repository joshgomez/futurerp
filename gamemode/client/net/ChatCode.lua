

net.Receive( "FRP_ChatCode", function( len )     

	local code 	= net.ReadString();
	local obj 	= net.ReadTable();

	if(code == "GAVE_MONEY") then 
	
		chat.AddText(obj.color,obj.nick,
		Color(255,255,255)," have given ",
		Color(200,200,100),tostring(obj.money) .. FRP.currency,
		Color(255,255,255)," to you.");
		
	elseif(code == "KILLED_NPC") then 
	
		chat.AddText(Color(255,255,255),"You have gained ",
		Color(0,255,0),tostring(obj.experience) .. "XP ",
		Color(255,255,255)," and ",
		Color(200,200,100),tostring(obj.money) .. FRP.currency,
		Color(255,255,255)," for killing ", 
		Color(255,0,0),obj.name,
		Color(255,255,255),".");
	elseif(code == "WELCOME") then 

		chat.AddText(Color(255,255,255),"Welcome ",
		obj.color,obj.nick,
		Color(255,255,255)," to ",
		Color(0,148,255),FRP.name,
		Color(255,255,255)," server, for more information press ",
		Color(255,0,0),"F4", 
		Color(255,255,255),".");
	
	end
	
end ) 