
AddCSLuaFile( "shared.lua" )

AddCSLuaFile("client/fonts.lua")

AddCSLuaFile("client/hooks/HUDPaint.lua")
AddCSLuaFile("client/hooks/HUDShouldDraw.lua")
AddCSLuaFile("client/hooks/OnEntityCreated.lua")

AddCSLuaFile("client/net/ChatCode.lua")

AddCSLuaFile("client/umhooks/npc/shopmenus/weapon.lua")
AddCSLuaFile("client/umhooks/npc/shopmenus/ammo.lua")
AddCSLuaFile("client/umhooks/npc/shopmenus/health.lua")
AddCSLuaFile("client/umhooks/npc/shopmenus/armor.lua")
AddCSLuaFile("client/umhooks/npc/shopmenus/food.lua")
AddCSLuaFile("client/umhooks/BuyStatus.lua")
AddCSLuaFile("client/umhooks/ShowMenu.lua")

AddCSLuaFile( "cl_init.lua" )

include( "shared.lua" )

include( "server/runCommands.lua" )

FRP.Player 	= {};
FRP.DB 		= {};

FRP.firstSpawn = false;
FRP.players 		= {};

FRP.NPC = {};
FRP.NPC.timer = 0;
FRP.NPC.lastSpawnpoint = 0;

include( "server/resources.lua" );

include( "server/database.lua" );
include( "server/database/player.lua" );
include( "server/database/npc.lua" );

include( "server/player/level.lua" );
include( "server/player/experience.lua" );
include( "server/player/money.lua" );
include( "server/player/fame.lua" );
include( "server/player/energy.lua" );
include( "server/player/hunger.lua" );
include( "server/player.lua" )

include( "server/npc.lua" )
include( "server/main.lua" )

include( "server/shops.lua" )
include( "server/shops/weapon.lua" )
include( "server/shops/ammo.lua" )
include( "server/shops/health.lua" )
include( "server/shops/armor.lua" )
include( "server/shops/food.lua" )

include( "server/overrides/PlayerSpawn.lua" )
include( "server/overrides.lua" )

include( "server/hooks/PlayerInitialSpawn.lua" )
include( "server/hooks/PlayerSpawn.lua" )
include( "server/hooks/PlayerLoadout.lua" )
include( "server/hooks/PlayerDisconnected.lua" )
include( "server/hooks/PlayerHurt.lua" )
include( "server/hooks/PlayerDeath.lua" )
include( "server/hooks/OnNPCKilled.lua" )
include( "server/hooks/Think.lua" )
include( "server/hooks/ShutDown.lua" )
include( "server/hooks/InitPostEntity.lua" )
include( "server/hooks.lua" )

include( "server/consoleCommands.lua" )

concommand.Add( "frp_player_reset"			,function(ply, command, arguments) return FRP.Command:PlayerReset(ply, command, arguments) end )
concommand.Add( "frp_give_money"			,function(ply, command, arguments) return FRP.Command:GiveMoney(ply, command, arguments) end )
concommand.Add( "frp_drop_weapon"		,function(ply, command, arguments) return FRP.Command:DropWeapon(ply, command, arguments) end )

hook.Add( "PlayerInitialSpawn"				,"frp_PlayerInitialSpawn"		,function(ply) return FRP:PlayerInitialSpawn(ply) end)
hook.Add( "PlayerSpawn"						,"frp_PlayerSpawn"				,function(ply) return FRP:PlayerSpawn(ply) end)
hook.Add( "PlayerLoadout"					,"frp_PlayerLoadout"			,function(ply) return FRP:PlayerLoadout(ply) end)

hook.Add( "PlayerDisconnected"			,"frp_PlayerDisconnected"		,function(ply) return FRP:PlayerDisconnected(ply) end)
hook.Add( "ShowSpare2"						,"frp_ShowSpare2"				,function(ply) return FRP:ShowSpare2(ply) end)
hook.Add( "PlayerHurt"							,"frp_PlayerHurt"					,function(victim, attacker, healthRemaining, damageTaken) return FRP:PlayerHurt(victim, attacker, healthRemaining, damageTaken) end)
hook.Add( "PlayerDeath"						,"frp_PlayerDeath"				,function(victim, inflictor, attacker) return FRP:PlayerDeath(victim, inflictor, attacker) end)
hook.Add( "OnNPCKilled"						,"frp_OnNPCKilled"				,function(npc, attacker, inflictor) return FRP:OnNPCKilled(npc, attacker, inflictor) end)
hook.Add( "Think" 								,"frp_Think"						,function() return FRP:Think() end);
hook.Add( "ShutDown" 						,"frp_ShutDown"					,function() return FRP:ShutDown() end);
hook.Add( "InitPostEntity"					,"frp_InitPostEntity"				,function() return FRP:InitPostEntity() end)
hook.Add( "FRP_FirstSpawn"					,"frp_FirstSpawn"					,function() return FRP:FirstSpawn() end)
