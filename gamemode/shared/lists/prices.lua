--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()

FRP.Lists.Prices = {}

FRP.Lists.Prices.weapon = {}
FRP.Lists.Prices.weapon["weapon_pistol"] 			= 1000;
FRP.Lists.Prices.weapon["weapon_357"] 				= 3500;
FRP.Lists.Prices.weapon["weapon_smg1"] 			= 5000;
FRP.Lists.Prices.weapon["weapon_crossbow"] 		= 9000;
FRP.Lists.Prices.weapon["weapon_rpg"] 				= 12000;
FRP.Lists.Prices.weapon["weapon_frag"] 				= 350;
FRP.Lists.Prices.weapon["weapon_shotgun"] 		= 6000;
FRP.Lists.Prices.weapon["weapon_ar2"] 				= 8000;

FRP.Lists.Prices.ammo = {}
FRP.Lists.Prices.ammo["Pistol"] 							= 100;
FRP.Lists.Prices.ammo["357"] 							= 150;
FRP.Lists.Prices.ammo["SMG1"] 							= 200;
FRP.Lists.Prices.ammo["XBowBolt"] 					= 300;
FRP.Lists.Prices.ammo["RPG_Round"] 					= 400;
FRP.Lists.Prices.ammo["Grenade"] 						= 350;
FRP.Lists.Prices.ammo["Buckshot"] 					= 250;
FRP.Lists.Prices.ammo["AR2"] 							= 300;
FRP.Lists.Prices.ammo["SMG1_Grenade"] 			= 325;
FRP.Lists.Prices.ammo["AR2AltFire"] 					= 350;

FRP.Lists.Prices.health = {}
FRP.Lists.Prices.health["item_frp_health_bandage"] 		= 100;
FRP.Lists.Prices.health["item_frp_health_painkiller"] 		= 150;
FRP.Lists.Prices.health["item_frp_health_firstaid"] 			= 200;
FRP.Lists.Prices.health["item_frp_health_morphine"] 		= 250;

FRP.Lists.Prices.armor = {}
FRP.Lists.Prices.armor["item_frp_armor_flak_jacket"] 			= 300;
FRP.Lists.Prices.armor["item_frp_armor_kevlar_light"] 			= 500;
FRP.Lists.Prices.armor["item_frp_armor_kevlar_heavy"] 		= 700;
FRP.Lists.Prices.armor["item_frp_armor_nano"] 					= 1000;

FRP.Lists.Prices.food = {}
FRP.Lists.Prices.food["item_frp_food_snacks"] 					= 1;
FRP.Lists.Prices.food["item_frp_food_protein_pill"] 				= 3;
FRP.Lists.Prices.food["item_frp_food_protein_bar"] 				= 5;


