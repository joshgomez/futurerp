--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()

FRP.Lists.Shops = {}

FRP.Lists.Shops.ammo = {}
FRP.Lists.Shops.ammo["Pistol"] 						= 18;
FRP.Lists.Shops.ammo["357"] 						= 6;
FRP.Lists.Shops.ammo["SMG1"] 						= 45;
FRP.Lists.Shops.ammo["XBowBolt"] 				= 1;
FRP.Lists.Shops.ammo["RPG_Round"] 			= 1;
FRP.Lists.Shops.ammo["Grenade"] 					= 1;
FRP.Lists.Shops.ammo["Buckshot"] 				= 6;
FRP.Lists.Shops.ammo["AR2"] 						= 30;
FRP.Lists.Shops.ammo["SMG1_Grenade"] 		= 1;
FRP.Lists.Shops.ammo["AR2AltFire"] 				= 1;

FRP.Lists.Shops.health = {}
FRP.Lists.Shops.health["item_frp_health_bandage"] 		= 20;
FRP.Lists.Shops.health["item_frp_health_painkiller"] 		= 30;
FRP.Lists.Shops.health["item_frp_health_firstaid"] 			= 40;
FRP.Lists.Shops.health["item_frp_health_morphine"] 		= 60;

FRP.Lists.Shops.armor = {}
FRP.Lists.Shops.armor["item_frp_armor_flak_jacket"] 				= 30;
FRP.Lists.Shops.armor["item_frp_armor_kevlar_light"] 			= 50;
FRP.Lists.Shops.armor["item_frp_armor_kevlar_heavy"] 			= 60;
FRP.Lists.Shops.armor["item_frp_armor_nano"] 						= 100;

FRP.Lists.Shops.food = {}
FRP.Lists.Shops.food["item_frp_food_snacks"] 				= 10;
FRP.Lists.Shops.food["item_frp_food_protein_pill"] 			= 30;
FRP.Lists.Shops.food["item_frp_food_protein_bar"] 			= 50;