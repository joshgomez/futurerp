--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()

FRP.Lists.clipsize = {}

FRP.Lists.clipsize["weapon_pistol"] 			= 18;
FRP.Lists.clipsize["weapon_357"] 				= 6;
FRP.Lists.clipsize["weapon_smg1"] 			= 45;
FRP.Lists.clipsize["weapon_crossbow"] 		= 10;
FRP.Lists.clipsize["weapon_rpg"] 				= 3;
FRP.Lists.clipsize["weapon_frag"] 				= 5;
FRP.Lists.clipsize["weapon_shotgun"] 		= 6;
FRP.Lists.clipsize["weapon_ar2"] 				= 30;