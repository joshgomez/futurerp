--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()

FRP.Lists.Blacklists = {}

FRP.Lists.Blacklists.tool = {
	"item_ammo_crate",
	"env_headcrabcanister",
	"prop_thumper",
	"npctool_health",
	"npctool_spawner",
	"npctool_follower",
	"npctool_controller",
	"npctool_notarget",
	"npctool_proficiency",
	"npctool_viewcam",
	"npctool_relationships",
	"dynamite",
	"duplicator"
}

FRP.Lists.Blacklists.property = {"ignite","drive"}

FRP.Lists.Blacklists.prop = {
	"models/props_junk/gascan001a.mdl",
	"models/props_c17/oildrum001_explosive.mdl",
	"models/props_junk/propane_tank001a.mdl"
}
