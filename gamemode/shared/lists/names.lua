--[[

	Dependency:
		shared.lua

]]

AddCSLuaFile();

FRP.Lists.names = {}

FRP.Lists.names["npc_frp_shop_weapon"] 				= "Jack the Weapon Merchant";
FRP.Lists.names["npc_frp_shop_armor"] 				= "John the Armor Merchant";
FRP.Lists.names["npc_frp_shop_health"] 				= "Dr. Karin Chakwas";
FRP.Lists.names["npc_frp_shop_ammo"] 				= "James the Ammo Merchant";
FRP.Lists.names["npc_frp_shop_food"] 					= "Chef Anna Hartnett";

FRP.Lists.names["npc_alyx"] 								= "Alyx Vance";
FRP.Lists.names["npc_antlion"] 							= "Antlion";
FRP.Lists.names["npc_antlion_template_maker"] 	= "Antlion";
FRP.Lists.names["npc_antlionguard"] 					= "Antlion Guard";
FRP.Lists.names["npc_barnacle"] 							= "Barnacle";
FRP.Lists.names["npc_barney"] 							= "Barney Calhoun";
FRP.Lists.names["npc_breen"] 								= "Dr. Wallace Breen";
FRP.Lists.names["npc_citizen"] 								= "Citizen";
FRP.Lists.names["npc_combine_camera"] 			= "Combine Camera";
FRP.Lists.names["npc_combine_s"] 						= "Combine Solider";
FRP.Lists.names["npc_combinedropship"] 				= "Combine Dropship";
FRP.Lists.names["npc_combinegunship"] 				= "Combine Gunship";
FRP.Lists.names["npc_crabsynth"] 						= "Crab Synth";
FRP.Lists.names["npc_cranedriver"] 						= "Crane";
FRP.Lists.names["npc_crow"] 								= "Crow";
FRP.Lists.names["npc_cscanner"] 							= "City Scanner";
FRP.Lists.names["npc_dog"] 								= "Dog";
FRP.Lists.names["npc_eli"] 									= "Dr. Eli Vance";
FRP.Lists.names["npc_fastzombie"] 						= "Fast Zombie";
FRP.Lists.names["npc_fisherman"] 						= "The Fisherman";
FRP.Lists.names["npc_gman"] 								= "G-man";
FRP.Lists.names["npc_headcrab"] 						= "Headcrab";
FRP.Lists.names["npc_headcrab_poison"] 					= "Poison Headcrab";
FRP.Lists.names["npc_headcrab_fast"] 					= "Fast Headcrab";
FRP.Lists.names["npc_helicopter"] 						= "Helicopter";
FRP.Lists.names["npc_ichthyosaur"] 						= "Ichthyosaur";
FRP.Lists.names["npc_kleiner"] 							= "Dr. Isaac Kleiner";
FRP.Lists.names["npc_manhack"] 						= "Manhack";
FRP.Lists.names["npc_metropolice"] 					= "Metropolice";
FRP.Lists.names["npc_monk"] 								= "Father Grigori";
FRP.Lists.names["npc_mortarsynth"] 					= "Mortar Synth";
FRP.Lists.names["npc_mossman"] 						= "Judith Mossman";
FRP.Lists.names["npc_pigeon"] 							= "Pigeon";
FRP.Lists.names["npc_poisonzombie"] 					= "Poison Zombie";
FRP.Lists.names["npc_rollermine"] 						= "Rollermine";
FRP.Lists.names["npc_seagull"] 							= "Seagull";
FRP.Lists.names["npc_sniper"] 								= "Sniper";
FRP.Lists.names["npc_stalker"] 							= "Stalker";
FRP.Lists.names["npc_strider"] 								= "Strider";
FRP.Lists.names["npc_turret_ceiling"] 					= "Ceiling turret";
FRP.Lists.names["npc_turret_floor"] 						= "Floor turret";
FRP.Lists.names["npc_turret_ground"] 					= "Ground turret";
FRP.Lists.names["npc_vortigaunt"] 						= "Vortigaunt";
FRP.Lists.names["npc_zombie"] 							= "Zombie";
FRP.Lists.names["npc_zombie_torso"] 					= "Torso Zombie";

FRP.Lists.names["weapon_crowbar"] 		= "Crowbar";
FRP.Lists.names["weapon_pistol"] 			= "HK USP Match";
FRP.Lists.names["weapon_357"] 				= ".357 Magnum";
FRP.Lists.names["weapon_smg1"] 			= "MP7 Submachine Gun";
FRP.Lists.names["weapon_crossbow"] 		= "Crossbow";
FRP.Lists.names["weapon_rpg"] 				= "RPG";
FRP.Lists.names["weapon_frag"] 				= "Fragmentation Grenade";
FRP.Lists.names["weapon_shotgun"] 		= "SPAS-12 Shotgun";
FRP.Lists.names["weapon_ar2"] 				= "AR2 OSI Pulse Rifle";

FRP.Lists.names["Pistol"] 						= "9MM Pistol Rounds";
FRP.Lists.names["357"] 							= ".357 Magnum Rounds";
FRP.Lists.names["SMG1"] 						= "HK 4.6×30mm Rounds";
FRP.Lists.names["XBowBolt"] 					= "Heated Rebar";
FRP.Lists.names["RPG_Round"] 				= "Laser-guided Rocket";
FRP.Lists.names["Grenade"] 					= "MK3A2 Grenade";
FRP.Lists.names["Buckshot"] 					= "12-gauge buckshot shells";
FRP.Lists.names["AR2"] 							= "Pulse magazine";
FRP.Lists.names["SMG1_Grenade"] 		= "Rifle Grenade ";
FRP.Lists.names["AR2AltFire"] 				= "Energy Ball";

FRP.Lists.names["item_frp_health_bandage"] 				= "Bandage";
FRP.Lists.names["item_frp_health_painkiller"] 			= "Painkiller Pill";
FRP.Lists.names["item_frp_health_firstaid"] 				= "First Aid Kit";
FRP.Lists.names["item_frp_health_morphine"] 			= "Morphine Syringe";

FRP.Lists.names["item_frp_armor_flak_jacket"] 				= "Flak Jacket";
FRP.Lists.names["item_frp_armor_kevlar_light"] 				= "Light Kevlar Vest";
FRP.Lists.names["item_frp_armor_kevlar_heavy"] 			= "Heavy Kevlar Vest";
FRP.Lists.names["item_frp_armor_nano"] 						= "Nanometal Vest";

FRP.Lists.names["item_frp_food_snacks"] 					= "Snacks";
FRP.Lists.names["item_frp_food_protein_pill"] 			= "Protein Pill";
FRP.Lists.names["item_frp_food_protein_bar"] 			= "Protein Bar";


