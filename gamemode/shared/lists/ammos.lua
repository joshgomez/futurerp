--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()

FRP.Lists.ammos = {}
FRP.Lists.ammos["weapon_pistol"] 				= "Pistol";
FRP.Lists.ammos["weapon_357"] 				= "357";
FRP.Lists.ammos["weapon_smg1"] 				= "SMG1";
FRP.Lists.ammos["weapon_crossbow"] 		= "XBowBolt";
FRP.Lists.ammos["weapon_rpg"] 				= "RPG_Round";
FRP.Lists.ammos["weapon_frag"] 				= "Grenade ";
FRP.Lists.ammos["weapon_shotgun"] 			= "Buckshot";
FRP.Lists.ammos["weapon_ar2"] 				= "AR2";

FRP.Lists.ammos2 = {}
FRP.Lists.ammos2["weapon_smg1"] 			= "SMG1_Grenade";
FRP.Lists.ammos2["weapon_ar2"] 				= "AR2AltFire";

