--[[ 

	Dependency:
		shared.lua

]]

AddCSLuaFile()
function FRP:Print(msg)
	print("[".. FRP.prefix .. "]: ".. msg) 
end

function FRP:ChatPrint(ply,msg) 
	ply:ChatPrint("[".. FRP.prefix .. "]: ".. msg)
end

function FRP:NumberToMoney(amount)
	local formatted = amount
	while true do  
		formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
		if (k==0) then
		  break
		end
	end
	
	return formatted
end

function FRP:FindPlayer(info)
	
	local all = player.GetAll();

	for k, v in pairs(all) do
	
		if(tonumber(info) == v:UserID()) then
			return v
		end
	end

	for k, v in pairs(all) do
		if (string.find(string.lower(v:Name()), string.lower(tostring(info))) != nil) then
			return v
		end
	end
	
	return nil
end