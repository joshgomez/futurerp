
AddCSLuaFile()

function GM:CreateTeams()

	if ( !GAMEMODE.TeamBased ) then return end
	
	TEAM_HUMAN = 1
	team.SetUp( TEAM_HUMAN, "Human", Color( 0, 0, 255 ) )
	team.SetSpawnPoint( TEAM_HUMAN, "info_player_start" )
	
	TEAM_ELVES = 2
	team.SetUp( TEAM_ELVES, "Elves", Color( 255, 150, 150 ) )
	team.SetSpawnPoint( TEAM_ELVES, "info_player_start" )
	
	team.SetSpawnPoint( TEAM_SPECTATOR, "worldspawn" )

end