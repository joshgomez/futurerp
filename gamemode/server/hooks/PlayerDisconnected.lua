--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		server/player.lua

]]

function FRP:PlayerDisconnected( ply )

	FRP.Player:SaveCondition(ply);
	FRP.Player:SaveWeaponAndAmmo(ply);
    FRP.DB.Player:SaveData(ply);
	FRP.Player:UnsetData(ply);
end