--[[ 

	Dependency:
		shared.lua
		server/player.lua

]]

function FRP:PlayerLoadout( ply )

	FRP.Player:LoadAmmo(ply);
	FRP.Player:LoadWeapon(ply);
	FRP.Player:LoadCondition(ply);
	
	GAMEMODE:SetPlayerSpeed( ply, 250, 250 ) 
	
end