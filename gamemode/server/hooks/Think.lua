--[[ 

	Dependency:
		shared.lua
		server/npc.lua

]]

function FRP:Think()

	if(!FRP.running) then return end

	--FrameTime()
	
	for _, v in pairs( player.GetAll() ) do
		if not(v:IsValid() or v:IsConnected()) then return end
			
		local id = v:UniqueID();
		if not (FRP.players[id]) then return end
		
		if(v:Alive()) then
		
			local usingEnergy = false;
			if(v:KeyDown(IN_SPEED)) then
			
				usingEnergy = true;
				if(FRP.players[id].energy > 0) then
					v:AddEnergy(-50*FrameTime());
					if(v:GetRunSpeed() < 500) then
						GAMEMODE:SetPlayerSpeed( v, 250, 500 );
					end
				end
				
				if(FRP.players[id].energy == 0 and v:GetRunSpeed() > 250) then
					GAMEMODE:SetPlayerSpeed( v, 250, 250 );
				end
			end
			
			if(FRP.players[id].hunger < 100) then
				v:AddHunger(0.05*FrameTime());
				
				if(FRP.players[id].energy < 100 and (!usingEnergy)) then
					v:AddEnergy(5*FrameTime());
				end
			else
				local hp = v:Health();
				if(hp > 30 and FRP.Player.timerHealth < CurTime()) then
					FRP.Player.timerHealth = CurTime() + 10;
					v:SetHealth(hp-1);
				end
			end
			
		end
		
	end
	
	
	if(FRP.firstSpawn) then
		if(FRP.FileLoaded.npcHostile and FRP.NPC.timerHostile < CurTime()) then
			local npcs = ents.FindByClass("npc_*");
			local npcsCount = table.Count( npcs);
			
			if(npcsCount < GetConVar("frp_npc_max"):GetInt() and FRP.NPC:RandomHostile()) then
				FRP.NPC.timerHostile = CurTime() + GetConVar("frp_npc_hostile_interval"):GetInt();
			end
		end
		
		if(FRP.FileLoaded.npcShop and FRP.NPC.timerShop < CurTime() and FRP.NPC:RandomShop()) then
			FRP.NPC.timerShop = CurTime() + GetConVar("frp_npc_shop_interval"):GetInt();
		end
	end
end
