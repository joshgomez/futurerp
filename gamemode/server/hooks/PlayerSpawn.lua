--[[ 

	Dependency:
		shared.lua
		player_class/player_frp_default.lua

]]

function FRP:PlayerSpawn( ply )

	if(!FRP.running) then return end
	if(!FRP.firstSpawn) then 
		FRP.firstSpawn = true
		hook.Call("FRP_FirstSpawn");
	end
	
	player_manager.SetPlayerClass( ply, "player_frp_default" );
	
end