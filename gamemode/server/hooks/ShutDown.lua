--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		server/player.lua

]]

function FRP:ShutDown()

	for _, v in pairs( player.GetAll() ) do
		
		if!(v:IsValid()) then return end
		
		FRP.Player:SaveCondition(v);
		FRP.Player:SaveWeaponAndAmmo(v);
		FRP.DB.Player:SaveData(v);
		FRP.Player:UnsetData(v);
	end
end