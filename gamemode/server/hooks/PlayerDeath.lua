--[[ 

	Dependency:
		shared.lua
		server/player.lua
		server/player/experience.lua

]]

function FRP:PlayerDeath( victim, inflictor, attacker )

	if(!FRP.running) then return end
	
	if(victim:IsPlayer()) then
	
		if(attacker:IsPlayer() and attacker != victim) then
			
			if(victim:Fame() >= 0) then 
				FRP:ChatPrint(attacker,"You have lost 1 fame for killing an innocent being.");
				attacker:AddFame(-1);
			else
				FRP:ChatPrint(attacker,"You have won 1 fame for killing a evil soul.");
				attacker:AddFame(1);
			end
		end
	
		victim:SetHunger(0.0);
		victim:SetEnergy(90.0);
		victim:SetFame(0);
		
		FRP.Player:ResetWeaponAndAmmo(victim)
		
		local varXP = GetConVar("frp_xp_death"); 
		if(varXP != nil) then
	 
			local xp = varXP:GetInt()*victim:Level();
			FRP:ChatPrint(victim,"You died! Lost " .. xp .. "XP.");
			victim:AddExperience(-xp);
		end
		FRP.DB.Player:SaveData(victim);
	end
end

