--[[ 

	Dependency:
		shared.lua
		server/npc.lua
		server/database/npc.lua

]]

function FRP:InitPostEntity()

	local data = FRP.DB.NPC:LoadShopData()
	if(data == nil) then return end
	
	for k, v in pairs(data) do
		
		local obj 		= {}
		obj.npc 		= v;
		obj.weapon 	= v.weapon or nil;
		obj.class 		= k;
		obj 				= FRP.NPC.RandomSpawnpoint(obj);
		
		local npcs = ents.FindByClass(k)
		if(next(npcs) == nil)then
		
			FRP.NPC:Spawn(obj);
		end
	end
	
	data = FRP.DB.NPC:LoadHostileData()
	if(data == nil) then return end
	
	for i, l in pairs(data) do
		
		for j, k in pairs(l.spawnpoints) do
			local npcs = ents.FindByClass("npc_*")
			if(table.Count(npcs) > GetConVar("frp_npc_max"):GetInt()) then break end
			
			local obj 		= {}
			obj.npc 		= l;
			obj.weapon 	= l.weapon or nil;
			obj.class 		= i;
			obj.pos 			= k;
			obj.point 		= j;
			obj.vec 			= util.StringToType(k.x .. " " .. k.y .. " " .. k.z, "Vector");
			
			FRP.NPC:Spawn(obj);
		end
	end
end