--[[ 

	Dependency:
		shared.lua
		server/player/experience.lua
		server/player/money.lua

]]

function FRP:OnNPCKilled(npc,attacker,inflictor)

	if(!FRP.running) then return end

	if(npc != attacker and attacker:IsPlayer()) then
	 
		local className = npc:GetClass();
		if(string.find(className,"npc_frp_shop") != nil) then
			
			FRP:ChatPrint(attacker,"You have lost 1 fame for killing an innocent being.");
			attacker:AddFame(-1);
		else
		
			local varXP = GetConVar("frp_" .. className .. "_xp"); 
			if(varXP == nil) then
				varXP = GetConVar("frp_xnpc_xp"); 
			end
			
			local varMoney = GetConVar("frp_" .. className .. "_money"); 
			if(varMoney == nil) then
					varMoney = GetConVar("frp_xnpc_money"); 
			end
			
			local name = "it";
			if(FRP.Lists.names[className]) then
				name = FRP.Lists.names[className];
			end
			
			local obj = {};
			obj.name 			= name
			obj.money 			= varMoney:GetString();
			obj.experience 	= varXP:GetString();
			
			FRP:PrintChatCode(attacker,"KILLED_NPC",obj);
			
			attacker:AddExperience(obj.experience);
			attacker:AddMoney(obj.money);
			attacker:GiveAmmo( 1, "Pistol" );
		end
	end
end


