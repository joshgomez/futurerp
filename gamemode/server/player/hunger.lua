--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/hunger.lua

]]

local meta = FindMetaTable("Player")
function meta:InitHunger(data)

	if (data == nil or data.hunger == nil) then
		self:SetHunger( 0.0 )
	else
		self:SetHunger( data.hunger )
	end
end
 
function meta:AddHunger(amount)

	local current = self:Hunger()
	self:SetHunger( current + amount )
end
 
function meta:SetHunger(amount)

	local hunger = amount or 0;
	hunger = math.min(100.0,amount);
	hunger = math.max(0.0,hunger);
	
	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].hunger = hunger;
	self:SetNetworkedFloat( "Hunger", hunger )
end