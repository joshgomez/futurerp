--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/energy.lua

]]

local meta = FindMetaTable("Player")
function meta:InitEnergy(data)

	if (data == nil or data.energy == nil) then
		self:SetEnergy( 90.0 )
	else
		self:SetEnergy( data.energy )
	end
end
 
function meta:AddEnergy(amount)

	local current = self:Energy()
	self:SetEnergy( current + amount )
end
 
function meta:SetEnergy(amount)

	local energy = amount or 0;
	energy = math.min(100.0,amount);
	energy = math.max(0.0,energy);
	
	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].energy = energy;
	self:SetNetworkedFloat( "Energy", energy )
end