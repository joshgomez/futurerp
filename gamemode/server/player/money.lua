--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/money.lua

]]

local meta = FindMetaTable("Player")
function meta:InitMoney(data)

	if (data == nil or data.money == nil) then
		local startMoney = GetConVar("frp_money_start"):GetInt();
		self:SetMoney( startMoney )
	else
		self:SetMoney( data.money )
	end
end
 
function meta:AddMoney(amount)

	local current = self:Money()
	self:SetMoney( current + amount )
end
 
function meta:SetMoney(amount)
	
	local mny = amount or 0;
	if(mny < 0) then mny = 0 end;

	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].money = mny;
	self:SetNetworkedInt( "Money", mny)
end

function meta:GiveMoney(target,amount)

	if(target == nil) then 
	
		FRP:ChatPrint(self,"Can't find the player.");
		return nil;
	end
	
	if(amount == nil) then 
	
		FRP:ChatPrint(self,"The amount is invalid.");
		return nil;
	end
	
	if not (target:IsValid() and target:IsPlayer()) then
		FRP:ChatPrint(self,"The player is invalid.");
		return nil;
	end	
	
	if(target == self) then
		FRP:ChatPrint(self,"You can't give money to yourself.");
		return nil;
	end
	
	local money = math.abs(tonumber(amount));
	if(money > self:Money()) then
	
		FRP:ChatPrint(self,"You don't have enough money.");
		return nil;
	end

	self:AddMoney(-money)
	target:AddMoney(money)
	
	FRP:ChatPrint(self,"You have given " ..money.. FRP.currency .. " to ".. target:Nick() ..".");
	
	return money;
end