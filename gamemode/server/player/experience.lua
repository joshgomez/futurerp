--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/experience.lua

]]

local meta = FindMetaTable("Player")
function meta:InitExperience(data)

	if (data == nil or data.experience == nil) then
		self:SetExperience( 0 )
	else
		self:SetExperience( data.experience )
	end
end
 
function meta:AddExperience(amount)

	local current = self:Experience()
	self:SetExperience( current + amount )
end
 
function meta:SetExperience(amount)

	local xp = amount or 0;
	if(xp < 0) then xp = 0 end
	
	if(xp >= self:NextLevel()) then
		xp = 0;
		self:AddLevel(1);
		FRP:ChatPrint(self,"You have levelled up!");
	end
	
	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].experience = xp;
	self:SetNetworkedInt( "Experience", xp )
end
 