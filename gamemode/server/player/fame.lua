--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/fame.lua

]]

local meta = FindMetaTable("Player")
function meta:InitFame(data)

	if (data == nil or data.fame == nil) then
		self:SetFame( 0 )
	else
		self:SetFame( data.fame )
	end
end
 
function meta:AddFame(amount)

	local current = self:Fame()
	self:SetFame( current + amount )
end
 
function meta:SetFame(amount)

	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].fame = amount;
	self:SetNetworkedInt( "Fame", amount )
end