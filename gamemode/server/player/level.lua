--[[ 

	Dependency:
		shared.lua
		server/database/player.lua
		shared/player/level.lua

]]

local meta = FindMetaTable("Player")

function meta:InitLevel(data)

	if (data == nil or data.level == nil) then
		self:SetLevel( 1 );
	else
		self:SetLevel( data.level );
	end
end
 
function meta:AddLevel(amount)

	local current = self:Level();
	self:SetLevel( current + amount );
end
 
function meta:SetLevel(amount)

	local lvl = amount or 1;
	if(lvl < 1) then lvl = 1 end

	local id = self:UniqueID();
	FRP.players[id] = FRP.players[id] or {}
	FRP.players[id].level = lvl;
	
	self:SetNetworkedInt( "Level", lvl );
	self:SetNextLevel();
end

function meta:SetNextLevel()

	self:SetNetworkedInt( "NextLevel", self:Level() *GetConVar("frp_xp_multiplier"):GetInt() );
end
 