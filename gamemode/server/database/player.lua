--[[ 

	Dependency:
		init.lua
		shared.lua
		server/database.lua
]]

FRP.DB.Player = {}

function FRP.DB.Player:SaveData(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	FRP.DB:SaveData("players/" .. id.. ".txt",FRP.players[id])
end

function FRP.DB.Player:LoadData(ply)

	return FRP.DB:LoadData("players/" .. ply:UniqueID().. ".txt");
end