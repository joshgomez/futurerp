--[[ 

	Dependency:
		init.lua
		shared.lua
		server/database.lua
]]

FRP.DB.NPC = {}

function FRP.DB.NPC:LoadHostileData()

	local data = FRP.DB:LoadData("npc/hostiles/" .. game.GetMap().. ".txt");
	if(data == nil) then
		FRP:Print("There is no NPC Hostile data for this map.")
	else
		FRP.FileLoaded.npcHostile 	= true;
	end

	return data;
end

function FRP.DB.NPC:LoadShopData()

	local data = FRP.DB:LoadData("npc/shops/" .. game.GetMap().. ".txt");
	if(data == nil) then
		FRP:Print("There is no NPC Shop data for this map.")
	else
		FRP.FileLoaded.npcShop 	= true;
	end
	
	return data;
end