--[[ 

	Dependency:
		shared.lua
		server/shops.lua
		server/player.lua
		shared/lists/prices.lua
		shared/lists/shops.lua
		
]]

net.Receive( 'FRP_BuyHealth', function(length,sender)

	local classShop 		= net.ReadString();
	local classItem 			= net.ReadString();
	local messageCode = "";
	
	if(FRP.Lists.Prices.health[classItem]) then
		local id = sender:UniqueID();
		local price = FRP.Lists.Prices.health[classItem];
		if(sender:Money() >= price) then
			
			local maxHealth = FRP.Player:GetMaxHealth(sender);
			if(sender:Health() < maxHealth) then
				sender:SetHealth(sender:Health() + FRP.Lists.Shops.health[classItem])
				if(sender:Health() > maxHealth) then
					sender:SetHealth(maxHealth);
				end
				
				sender:AddMoney(-price);
				FRP.Player:SaveCondition(sender);
				messageCode = "BOUGHT_ITEM";
			else
				messageCode = "CAN_NOT_BUY";
			end
		else
			messageCode = "NOT_ENOUGH_MONEY";
		end
	else
		messageCode = "ITEM_NOT_FOUND";
	end
	
	umsg.Start("FRP_BuyStatus", sender);
		umsg.String( classShop );
		umsg.String( classItem );
		umsg.String( messageCode );
	umsg.End();

end )