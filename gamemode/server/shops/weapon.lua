--[[ 

	Dependency:
		shared.lua
		server/shops.lua
		server/player.lua
		
]]

net.Receive( 'FRP_BuyWeapon', function(length,sender)

	local classShop 		= net.ReadString();
	local classWeapon 	= net.ReadString();
	
	local messageCode = "";
	
	if(FRP.Lists.Prices.weapon[classWeapon]) then
		local id = sender:UniqueID();
		if not (FRP.players[id].weapons[classWeapon]) then
			local price = FRP.Lists.Prices.weapon[classWeapon];
			if(sender:Money() >= price) then
			
				sender:Give( classWeapon );
				sender:AddMoney(-price);
				FRP.Player:SaveWeaponAndAmmo(sender);
				
				messageCode = "BOUGHT_ITEM"
			else
				messageCode = "NOT_ENOUGH_MONEY"
			end
		else
			messageCode = "ITEM_ALREADY_HAVE"
		end
	else
		messageCode = "ITEM_NOT_FOUND"
	end
	
	umsg.Start("FRP_BuyStatus", sender);
		umsg.String( classShop );
		umsg.String( classWeapon );
		umsg.String( messageCode );
	umsg.End();

end )