--[[ 

	Dependency:
		shared.lua
		server/shops.lua
		server/player.lua
		shared/lists/prices.lua
		shared/lists/shops.lua
		
]]

net.Receive( 'FRP_BuyArmor', function(length,sender)

	local classShop 		= net.ReadString();
	local classItem 			= net.ReadString();
	local messageCode = "";
	
	if(FRP.Lists.Prices.armor[classItem]) then
		local id = sender:UniqueID();
		local price = FRP.Lists.Prices.armor[classItem];
		if(sender:Money() >= price) then
			
			local armor = FRP.Lists.Shops.armor[classItem];
			local maxArmor = FRP.Player:GetMaxArmor(sender);
			if(sender:Armor() < maxArmor+armor) then
				sender:SetArmor(armor+maxArmor)
				sender:AddMoney(-price);
				FRP.Player:SaveCondition(sender);
				messageCode = "BOUGHT_ITEM";
			else
				messageCode = "CAN_NOT_BUY";
			end
		else
			messageCode = "NOT_ENOUGH_MONEY";
		end
	else
		messageCode = "ITEM_NOT_FOUND";
	end
	
	umsg.Start("FRP_BuyStatus", sender);
		umsg.String( classShop );
		umsg.String( classItem );
		umsg.String( messageCode );
	umsg.End();

end )