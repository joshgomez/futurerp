--[[ 

	Dependency:
		shared.lua
		server/shops.lua
		server/player.lua
		shared/lists/prices.lua
		shared/lists/weapons.lua
		shared/lists/shops.lua
		
]]


net.Receive( 'FRP_BuyAmmo', function(length,sender)

	local classShop 		= net.ReadString();
	local nameAmmo 		= net.ReadString();
	local messageCode = "";
	
	if(FRP.Lists.Prices.ammo[nameAmmo]) then
		local id = sender:UniqueID();
		local classWeapon = FRP.Lists.Weapon.byAmmo[nameAmmo];
		if(FRP.players[id].weapons[classWeapon]) then
			local price = FRP.Lists.Prices.ammo[nameAmmo];
			if(sender:Money() >= price) then
				
				sender:GiveAmmo(FRP.Lists.Shops.ammo[nameAmmo],nameAmmo);
				sender:AddMoney(-price);
				FRP.Player:SaveWeaponAndAmmo(sender);
				
				messageCode = "BOUGHT_ITEM"
			else
				messageCode = "NOT_ENOUGH_MONEY"
			end
		else
			messageCode = "ITEM_REQUIRED"
		end
	else
		messageCode = "ITEM_NOT_FOUND"
	end
	
	umsg.Start("FRP_BuyStatus", sender);
		umsg.String( classShop );
		umsg.String( nameAmmo );
		umsg.String( messageCode );
	umsg.End();

end )