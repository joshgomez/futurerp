--[[ 

	Dependency:
		shared.lua
		server/shops.lua
		server/player.lua
		shared/lists/prices.lua
		shared/lists/shops.lua
		
]]

net.Receive( 'FRP_BuyFood', function(length,sender)

	local classShop 		= net.ReadString();
	local classItem 			= net.ReadString();
	local messageCode = "";
	
	if(FRP.Lists.Prices.food[classItem]) then
		local id = sender:UniqueID();
		local price = FRP.Lists.Prices.food[classItem];
		if(sender:Money() >= price) then
			
			local food = FRP.Lists.Shops.food[classItem];

			sender:AddHunger(-food);
			sender:AddMoney(-price);
			
			messageCode = "BOUGHT_ITEM";
			
		else
			messageCode = "NOT_ENOUGH_MONEY";
		end
	else
		messageCode = "ITEM_NOT_FOUND";
	end
	
	umsg.Start("FRP_BuyStatus", sender);
		umsg.String( classShop );
		umsg.String( classItem );
		umsg.String( messageCode );
	umsg.End();

end )