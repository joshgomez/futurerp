function GM:PlayerSpawn( pl )

	if ( GAMEMODE.TeamBased && ( pl:Team() == TEAM_SPECTATOR || pl:Team() == TEAM_UNASSIGNED ) ) then

		GAMEMODE:PlayerSpawnAsSpectator( pl )
		return
	end

	pl:UnSpectate()
	pl:SetupHands()
	player_manager.OnPlayerSpawn( pl )
	player_manager.RunClass( pl, "Spawn" )

	hook.Call( "PlayerLoadout", GAMEMODE, pl )
	hook.Call( "PlayerSetModel", GAMEMODE, pl )
	
end