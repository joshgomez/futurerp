--[[ 

	Dependency:
		shared.lua
		shared/lists/blacklists.lua
		server/main.lua
		
]]

function GM:PlayerSpawnProp( ply, model )

	if(table.HasValue(FRP.Lists.Blacklists.prop,model) and not ply:IsAdmin()) then
		FRP:ChatPrint(ply,"Sorry you are not allowed to spawn this prop.");
		return false;
	end
	
	local cost = GetConVar("frp_prop_cost"):GetInt();
	if(ply:Money() < cost) then
		FRP:ChatPrint(ply,"Not enough money, it cost "..cost..FRP.currency.." to spawn a prop.");
		return false;
	end

	ply:AddMoney(-cost);
	return true;
end


function GM:PlayerSpawnSWEP(ply,weapon,info)
	FRP.PrintNotAllowed();
	return false;
end

function GM:PlayerGiveSWEP(ply,weapon,swep)
	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:PlayerSpawnVehicle(ply,model,name,obj)

	if(ply:IsAdmin( ) or table.HasValue(FRP.Lists.Whitelists.vehicle,name)) then
		return true;
	end

	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:PlayerSpawnRagdoll(ply,model)

	if(ply:IsAdmin( ) or table.HasValue(FRP.Lists.Whitelists.ragdoll,model)) then return true end

	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:PlayerSpawnNPC(ply,npc_type,weapon)

	if(ply:IsAdmin( )) then return true end

	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:PlayerSpawnSENT(ply,class)

	if(ply:IsAdmin( )) then return true end

	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:PlayerSpawnEffect(ply,model )

	if(ply:IsAdmin( )) then return true end

	FRP:PrintNotAllowed(ply);
	return false;
end

function GM:CanProperty(ply,property,ent)

	if(ply:IsAdmin( )) then return true end
	
	if(FRP:EntityAllowed(ent) and not table.HasValue(FRP.Lists.Blacklists.property,property)) then
		return true;
	end

	FRP:PrintNotAllowedEnt(ply);
	return false;
end

function GM:PhysgunPickup(ply,ent)
	local allowed = FRP:EntityAllowed(ent);
	if not (allowed) then
	
		if(timer.Exists( "FRP_PhysgunPickup" )) then
			timer.Destroy( "FRP_PhysgunPickup" )
		end
		
		local player = ply;
		timer.Create( "FRP_PhysgunPickup", 0.2, 1, function() FRP:PrintNotAllowedEnt(player); end) 
	end
	
	return allowed;
end

function GM:CanTool(ply,trace,tool ) 

	if(ply:IsAdmin( )) then return true end

	local allowed = false;
	if(trace.Entity or IsValid(trace.Entity)) then
		allowed = FRP:EntityAllowed(trace.Entity);
	end
	
	if(table.HasValue( FRP.Lists.Blacklists.tool, tool )) then
		allowed = false
	end
	
	if not (allowed) then
		FRP:PrintNotAllowedEnt(ply);
	end

	return allowed;
end
