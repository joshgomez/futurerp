--[[ 

	Dependency:
		init.lua
		shared.lua
		server/database/player.lua
		shared/lists/ammos.lua
		server/player/level.lua
		server/player/experience.lua
		server/player/money.lua

]]

local meta = FindMetaTable("Player")

function meta:GetTeamColor()
	
	local index = self:Team( );
	if(index) then
		return team.GetColor(index);
	end
	
	return Color(150,150,150)
end



function FRP.Player:SaveWeaponAndAmmo(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	for k, v in pairs( ply:GetWeapons() ) do
	
		local class = v:GetClass();
		
		FRP.players[id].weapons[class] 				= 1;
		
		FRP.players[id].ammos[class] 					= FRP.players[id].ammos[class] or {};
		FRP.players[id].ammos[class].clip 				= v:Clip1( );
		FRP.players[id].ammos[class].primary 		= ply:GetAmmoCount(v:GetPrimaryAmmoType());
		FRP.players[id].ammos[class].secondary 	= ply:GetAmmoCount(v:GetSecondaryAmmoType());
	end 
end

function FRP.Player:ResetWeaponAndAmmo(ply)
	
	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	FRP.players[id].weapons = {}
	FRP.players[id].ammos ={}
end

function FRP.Player:SaveCondition(ply)
	
	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	FRP.players[id].health = ply:Health();
	FRP.players[id].armor = ply:Armor();
	
end

function FRP.Player:LoadCondition(ply)
	
	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	local health = FRP.players[id].health;
	ply:SetMaxHealth(health);
	ply:SetHealth(health) ;
	FRP.players[id].health = FRP.Player:GetMaxHealth(ply);
	
	local armor = FRP.players[id].armor;
	ply:SetArmor(armor);
	FRP.players[id].armor = FRP.Player:GetMaxArmor(ply);
end

function FRP.Player:GetMaxHealth(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end

	local health = 100;
	health = health + FRP.players[id].level - 1;
	if(health > 150) then health = 150 end
	
	return health;
end

function FRP.Player:GetMaxArmor(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end

	local armor = 1 + FRP.players[id].level*2;
	if(armor > 200) then armor = 200 end
	
	return armor;
end

function FRP.Player:LoadAmmo(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	for k, v in pairs(FRP.players[id].ammos) do
	
		if(v.primary > 0) then
			ply:SetAmmo( v.primary, FRP.Lists.ammos[k] );
		end
		
		if(v.secondary > 0) then
			ply:SetAmmo( v.secondary, FRP.Lists.ammos2[k] );
		end
	end
end

function FRP.Player:LoadWeapon(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	
	for k, v in pairs(FRP.players[id].weapons) do
		if(k != "weapon_crowbar") then
			ply:Give( k );
		end
	end
end

function FRP.Player:UnsetData(ply)

	local id = ply:UniqueID();
	if not (FRP.players[id]) then return end
	FRP.players[id] = nil;
end

function FRP.Player:InitData(ply)

	local id = ply:UniqueID();
	FRP.players[id] = {};
	
	local data = FRP.DB.Player:LoadData(ply);
	
	ply:InitLevel(data);
	ply:InitExperience(data);
	ply:InitMoney(data);
	ply:InitFame(data);
	ply:InitEnergy(data);
	ply:InitHunger(data);
	
	FRP.players[id].weapons = {};
	if(data != nil and data.weapons != nil) then
		FRP.players[id].weapons = data.weapons;
	end
	
	FRP.players[id].ammos = {};
	if(data != nil and data.ammos != nil) then
		FRP.players[id].ammos = data.ammos;
	end
	
	FRP.players[id].health = FRP.Player:GetMaxHealth(ply);
	if(data != nil and data.health != nil and data.health > 0) then
		FRP.players[id].health = data.health;
	end
	
	FRP.players[id].armor = FRP.Player:GetMaxArmor(ply);
	if(data != nil and data.armor != nil and data.armor > 0) then
		FRP.players[id].armor = data.armor;
	end
	
end