--[[ 

	Dependency:
		shared.lua
		shared/util.lua
		server/player.lua
		
]]

function FRP:PrintNotAllowed(ply)
	FRP:ChatPrint(ply,"Sorry you are not allowed to use this feature.");
end

function FRP:PrintNotAllowedEnt(ply)
	FRP:ChatPrint(ply,"Sorry you are not allowed to change or move this Entity.");
end

util.AddNetworkString( "FRP_ChatCode" );
function FRP:PrintChatCode(target,code,tbl)      
	
	net.Start( "FRP_ChatCode" );
	net.WriteString( code );
	net.WriteTable( tbl );
	net.Send( target );
	
end

function FRP:EntityAllowed(ent)

	local class = ent:GetClass();

	if( class == "worldspawn" or 
		string.find(class,"prop_") == 1 or 
		string.find(class,"item_") == 1 or 
		string.find(class,"gmod_") == 1 or 
		string.find(class,"weapon_") == 1) then
		
		return true;
	end
	
	return false;
end

function FRP:Main()

	FRP.NPC.timerHostile 		= 	CurTime() + GetConVar("frp_npc_hostile_interval"):GetInt();
	FRP.NPC.timerShop 			= 	CurTime() + GetConVar("frp_npc_shop_interval"):GetInt();
	FRP.Player.timerHealth 		= 	CurTime() + 10;

	for _, v in pairs( player.GetAll() ) do
		if(v:IsConnected( )) then FRP.Player:InitData(v) end
	end
end