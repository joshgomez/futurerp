--[[ 

	Dependency:
		init.lua
		shared.lua
		server/database/npc.lua
		
]]

function FRP.NPC:Spawn(obj)

	local npc = ents.Create( obj.class );
	npc:SetCollisionGroup( COLLISION_GROUP_NPC );
	npc:SetPos( obj.vec );
	if(obj.pos.yaw != nil) then
		local yaw = util.StringToType( obj.pos.yaw , "Float" );
		npc:SetAngles(Angle(0,yaw,0));
	end
	
	if not (string.find(obj.class,"npc_frp_")) then
		npc:AddRelationship("npc_frp_shop_weapon D_FR 999");
		npc:AddRelationship("npc_frp_shop_ammo D_FR 999");
		npc:AddRelationship("npc_frp_shop_health D_FR 999");
		npc:AddRelationship("npc_frp_shop_armor D_FR 999");
		cleanup.ReplaceEntity(npc);
	end
	
	npc.FRP = {};
	npc.FRP.spawnpoint = obj.point;
	
	if(obj.weapon != nil or obj.pos.weapon) then
	
		obj.weapon = obj.pos.weapon or obj.weapon;
	
		npc:SetCurrentWeaponProficiency( WEAPON_PROFICIENCY_PERFECT );
		npc:SetKeyValue( "additionalequipment", obj.weapon );
		npc:SetKeyValue("spawnflags","8192");
	end
	
	npc:Spawn();
	

end

function FRP.NPC.RandomSpawnpoint(data)

	local obj = data or {}
	obj.pos = table.Random(obj.npc.spawnpoints);
	obj.point = table.KeyFromValue(obj.npc.spawnpoints,obj.pos);
	obj.vec = util.StringToType(obj.pos.x .. " " .. obj.pos.y .. " " .. obj.pos.z, "Vector");
	
	return obj;
end

function FRP.NPC.Random(data)

	local obj 		= {};
	obj.npc 		= table.Random(data);
	obj.weapon 	= obj.npc.weapon or nil;
	obj.class 		= table.KeyFromValue(data,obj.npc);
	obj 				= FRP.NPC.RandomSpawnpoint(obj);
	
	return obj;
end

function FRP.NPC.RandomShop()
	local data = FRP.DB.NPC:LoadShopData()
	if(data != nil) then
		local obj = FRP.NPC.Random(data);
		local npcs = ents.FindByClass(obj.class)
		if(next(npcs) == nil)then
		
			FRP.NPC:Spawn(obj);
			return true
		end
	end
	
	return false
end

function FRP.NPC.RandomHostile()
	local data = FRP.DB.NPC:LoadHostileData()
	if(data != nil) then
		local obj = FRP.NPC.Random(data);
		
		local found = false;
		for k, v in pairs(ents.FindByClass(obj.class)) do
			if(v.FRP and obj.point == v.FRP.spawnpoint) then 
				found = true;
				break 
			end
		end
		
		if(!found) then
			FRP.NPC:Spawn(obj);
			return true;
		end
	end
	
	return false;
end

