
function FRP.Command:PlayerReset(ply, command, arguments)
	
	local id = ply:UniqueID();
	FRP.players[id] = {};
	ply:KillSilent();
	FRP.DB.Player:SaveData(ply);
	FRP.Player:InitData(ply);
	
    FRP:ChatPrint(ply, "You player data have been deleted.");
end

function FRP.Command:GiveMoney(ply, command, arguments)
	
	local id = ply:UniqueID();
	if not(FRP.players[id]) then return end
	
	if not (arguments[1]) then
		FRP:ChatPrint(ply, "You must enter the player's name as the first argument.");
		return;
	end
	
	if not (arguments[2]) then
		FRP:ChatPrint(ply, "You must enter the amount of money as the second argument.");
		return;
	end
	
	if(ply:Level() < 5) then
		FRP:ChatPrint(ply, "You must be level 5 to give money.");
		return;
	end
	
	local target 	= FRP:FindPlayer(arguments[1]);
	local money 	= ply:GiveMoney(target,arguments[2]);
	if(money != nil) then
	
		local obj = {};
		obj.nick 	= ply:Nick()
		obj.color 	= ply:GetTeamColor();
		obj.money 	= money;
		
		FRP:PrintChatCode(target,"GAVE_MONEY",obj);
		
	end
end

function FRP.Command:DropWeapon(ply, command, arguments)
	
	local weapon = ply:GetActiveWeapon();
	if(weapon:IsValid()) then
	
		local class = weapon:GetClass();
		if(	class != "gmod_tool" and 
			class != "gmod_camera" and 
			class != "weapon_physgun" and 
			class != "weapon_physcannon" and 
			class != "weapon_crowbar") then
		
			if(ply:Level() < 5) then
				FRP:ChatPrint(ply, "You must be level 5 to drop your weapon.");
				return;
			end
		
			ply:DropWeapon(weapon);

		else
			FRP:ChatPrint(ply, "Cannot drop this weapon.");
		end
	end
end