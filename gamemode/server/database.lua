--[[ 

	Dependency:
		init.lua
		shared.lua
]]

function FRP.DB:SaveData(filepath,data)
	
	local json = util.TableToJSON(data);
	file.Write(FRP.folderName .. "/" .. filepath, json );
	
end

function FRP.DB:LoadData(filepath)

	local content = file.Read(FRP.folderName .. "/" .. filepath);
	if(content != nil) then
		local data = util.JSONToTable( content );
		if(data != nil and next(data) != nil) then
			return data;
		end
	end
	
	return nil;
end