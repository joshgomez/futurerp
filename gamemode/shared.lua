
GM.Name = "FutureRP";
GM.Author = "XzaR";
GM.Email = "Josh_Gomez90@hotmail.com";
GM.Website = "xuniver.se";

DeriveGamemode( "sandbox" );

--GM.TeamBased = true;
--GM.AllowSpectating = false
--GM.ValidSpectatorModes = { OBS_MODE_CHASE, OBS_MODE_IN_EYE, OBS_MODE_ROAMING }
--GM.ValidSpectatorEntities = { "player" }
--GM.CanOnlySpectateOwnTeam = false

GM.NoPlayerSuicide 				= false
GM.NoPlayerDamage 				= false
GM.NoPlayerSelfDamage 			= false
GM.NoPlayerTeamDamage 			= false
GM.NoPlayerPlayerDamage 		= false
GM.NoNonPlayerPlayerDamage 		= false
GM.NoPlayerFootsteps 			= false
GM.PlayerCanNoClip 				= false

include( "frp.lua" );

include( "shared/lists/blacklists.lua" );
include( "shared/lists/whitelists.lua" );
include( "shared/lists/ammos.lua" );
include( "shared/lists/names.lua" );
include( "shared/lists/prices.lua" );
include( "shared/lists/clipsizes.lua" );
include( "shared/lists/shops.lua" );
include( "shared/lists/weapons.lua" );
include( "shared/util.lua" );

include( "player_class/player_frp_default.lua" );

include( "shared/player/level.lua" );
include( "shared/player/experience.lua" );
include( "shared/player/money.lua" );
include( "shared/player/fame.lua" );
include( "shared/player/energy.lua" );
include( "shared/player/hunger.lua" );

include( "shared/overrides/CreateTeams.lua" );
include( "shared/overrides.lua" );

include( "shared/hooks/Initialize.lua" );
include( "shared/hooks/PlayerNoClip.lua" );

hook.Add( "PlayerNoClip", "frp_PlayerNoClip", function(ply) return FRP:PlayerNoClip(ply) end);
hook.Add( "Initialize" ,"frp_Initialize", function() return FRP:Initialize() end);