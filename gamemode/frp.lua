
AddCSLuaFile()

--Shared
FRP = {};
FRP.name 				= GM.Name;
FRP.author 				= GM.Author;
FRP.website 			= GM.Website;

FRP.description = [===[
A garry's mod gamemode which add some RP elements with a 
bit of RPG feature. Everything is persistent on disconnect 
but you will lose your weapon, ammo, armor, hunger and fame 
on death, some experience is also lost otherwise everything 
is saved. The goal is to never die and play fair.
 
There are NPCs on the map that sell the things you need.
The NPCs are killable but you will lose fame doing it and 
they spawn again later.

If hunger reach 100% your energy regeneration will stop 
and lose one health each ten seconds till you reach 30. 
 
You will also lose fame if you kill someone who is 
neutral or positive but gain it by killing someone 
who is negative.
 
Each level will give you a higher base health and armor. 
To get experience you can kill hostile NPCs.
]===]


FRP.prefix 		= "FRP"
FRP.currency 	= "$"
FRP.folderName 	= "futurerp"
FRP.running 	= false;

FRP.Lists = {}
FRP.Temp = {}

--Server
FRP.Player 			= {};
FRP.DB 				= {};
FRP.Command 	= {};

FRP.Player.timerHealth = 0;

FRP.firstSpawn = false;
FRP.players 		= {};

FRP.NPC = {};
FRP.NPC.ShopMenu = {}

FRP.NPC.timerHostile 		= 0;
FRP.NPC.timerShop 			= 0;

FRP.FileLoaded = {}
FRP.FileLoaded.npcHostile 	= false;
FRP.FileLoaded.npcShop 	= false;

--Client
FRP.Temp.clipSize = {}
